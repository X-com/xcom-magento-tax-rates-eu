This repository contains EU tax rates for when your webshop's origin is in the Netherlands and tax calculation should be based on Dutch tax rates when selling within the EU. When you need exceptions to specific situations for when for example revenue threshold is reached with a specific country, please create a new file.

# Files
- tax_rates_eu_origin_nl.csv (all rates NL based within EU)
- tax_rates_extended_origin_nl.csv (all rates NL based within EU and some other countries)